<?php
require_once("models/plant.php");

//GET method
if($_SERVER['REQUEST_METHOD']=='GET')
{
   if(isset($_GET['idPlant']))
   {
       try{
            $plant =  new Plant($_GET['idPlant']);
            echo json_encode(array(
                'status' => 0,
                'plant' => json_decode($plant->toJson())
            ));
       }catch(RecordNotFoundException $ex){
            echo json_encode(array(
                'status' => 3,
                'message' => $ex->getMessage()
            ));
       }
   }
   else 
   {
       echo json_encode(array(
            'status' => 0,
            'plants' => json_decode(Plant::getAllToJson())
        ));
   }
}