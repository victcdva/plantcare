<?php
		//get request URI
	$requestUri=$_SERVER['REQUEST_URI'];
	
	//split uriParts
	$uriParts=explode('/',$requestUri);
	//get uri info
	$controller=$uriParts[sizeof($uriParts) - 2];
	$parameters=$uriParts[sizeof($uriParts)-1];
	//send controller
	switch($controller)
	{
		case strtolower('plant'): require_once('plantcontroller.php');break;
		case strtolower('station'): require_once('stationcontroller.php');break;
		
		default:
			echo json_encode(array(
			'status'=>999,
			'errorMessage'=>'Invalid Controller'
			));
	}

	
	
?>
