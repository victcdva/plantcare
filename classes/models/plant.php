<?php
require_once("connection.php");
require_once("exceptions/recordnotfoundexception.php");
require_once("station.php");

class Plant
{
	private $idPlant;
	private $idStation;
	private $temperature;
	private $moisture;
    private $dateT;

    public function getId(){ return $this->idPlant; }
	public function setId($idPlant){ $this->idPlant=$idPlant; }
	public function getIdStation(){ return $this->idStation;}
	public function setIdStation($idStation){$this->idStation=$idStation; }
	public function getTemperature(){ return $this->temperature; }
	public function setTemperature($temperature){ $this->temperature=$temperature; }
    public function getMoisture(){ return $this->moisture; }
    public function setMoisture($moisture){ $this->moisture=$moisture; }
    public function getDateT(){ return $this->dateT; }
    public function setDateT($dateT){$this->dateT=$dateT; }
    
    public function __construct()
    {
        if(func_num_args())
        {
			$this->idPlant = 0;
			$this->idStation = 0;
            $this->temperature = 0;
            $this->moisture = 0;
            $this->dateT = "";
        }

        if(func_num_args() == 1)
		{
			$connection = MySqlConnection::getConnection();
			$query = "select idPlant, idStation, temperature, moisture, dateT from plant where idPlant = ?";
			$command = $connection->prepare($query);
			$command->bind_param('i', $j);
			$j=func_get_arg(0);
			$command->execute();
			$command->bind_result($idPlant, $idStation, $temperature, $moisture, $dateT);

			if ($command->fetch()) 
			{
				$this->idPlant = $idPlant;
				$this->idStation = $idStation; 
                $this->temperature = $temperature;
                $this->moisture = $moisture;
                $this->dateT = $dateT;
			}
			else
				throw new RecordNotFoundException(func_get_arg(0));
			mysqli_stmt_close($command);
			$connection->close();			
		}

		if (func_num_args() == 5) 
		{           
			$this->idPlant = func_get_arg(0);
			$this->idStation = func_get_arg(1);
            $this->temperature = func_get_arg(2);
            $this->moisture = func_get_arg(3);
            $this->dateT = func_get_arg(4);
		}
    }

	public function add() 
	{
        $connection = MysqlConnection::getConnection();
        $statement = "insert into plant(idStation, temperature, moisture, dateT) values (?, ?, ?, ?)";
        $command = $connection->prepare($statement);
        $command->bind_param('idds', $idStation, $temperature, $moisture, $dateT);        
		$idStation = $this->idStation;
		$temperature = $this->temperature;
        $moisture = $this->moisture;
        $dateT = $this->dateT;
        $command->execute();
        $affected = $command->affected_rows;
        $this->idPlant = $command->insert_id;
        mysqli_stmt_close($command);
        $connection->close();

        return ($affected > 0);
	}
	
	public static function getAll() {
		$array = array();
		$connection = MysqlConnection::getConnection();
		$query = 'select idPlant, idStation, temperature, moisture, dateT from plant';
		$command = $connection->prepare($query);
		$command->execute();
		$command->bind_result($idPlant, $idStation, $temperature, $moisture, $dateT);
		while($command->fetch()) {
			array_push($array, new Plant($idPlant));
		}
		mysqli_stmt_close($command);
		$connection->close();
		return $array;
	}

	public static function getAllToJson()
    {
        $jsonArray = array();
        foreach (self::getAll() as $item) 
        {
            array_push($jsonArray, json_decode($item->toJson(), true));
        }
        return json_encode($jsonArray);
	}
	
    public function toJson()
	{
		return json_encode(array(
			'id'=>$this->idPlant,
			'station'=>$this->idStation,
			'temperature'=>$this->temperature,
            'moisture'=>$this->moisture,
            'date'=>$this->dateT,
		));
	}
}
?>