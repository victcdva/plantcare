<?php
require_once("connection.php");
require_once("exceptions/recordnotfoundexception.php");

class Station
{
    private $idStation;
    private $description;

    public function getId(){ return $this->idStation;}
	public function setId($idStation){$this->idStation=$idStation;}
	public function getDescription(){ return $this->idStation;}
	public function setDescription($description){ $this->description=$description;}
    
    public function __construct()
    {
        if(func_num_args())
        {
            $this->idStation = 0;
            $this->description = "";
        }

        if(func_num_args() == 1)
		{
			$connection = MySqlConnection::getConnection();
			$query = "select idStation, description from station where idStation = ?";
			$command = $connection->prepare($query);
			$command->bind_param('i', $j);
			$j=func_get_arg(0);
			$command->execute();
			$command->bind_result($idStation, $description);

			if ($command->fetch()) 
			{				
                $this->idStation = $idStation;
                $this->description = $description;                
			}
			else
				throw new RecordNotFoundException(func_get_arg(0));
			mysqli_stmt_close($command);
			$connection->close();			
		}

		if (func_num_args() == 2) 
		{           
            $this->idStation = func_get_arg(0);            
            $this->description = func_get_arg(1);
		}
    }

	public function add() 
	{
        $connection = MysqlConnection::getConnection();
        $statement = "insert into plant(idStation, description) values (?, ?)";
        $command = $connection->prepare($statement);
        $command->bind_param('is', $idStation, $description);
        $idStation = $this->name->getId();
        $description = $this->description;        
        $command->execute();
        $affected = $command->affected_rows;
        $this->idPlant = $command->insert_id;
        mysqli_stmt_close($command);
        $connection->close();

        return ($affected > 0);
	}
	
	public static function getAll() {
		$array = array();
		$connection = MysqlConnection::getConnection();
		$query = 'select idStation, description from station';
		$command = $connection->prepare($query);
		$command->execute();
		$command->bind_result($idStation, $description);
		while($command->fetch()) {
			array_push($array, new Station($idStation));
		}
		mysqli_stmt_close($command);
		$connection->close();
		return $array;
	}

    public static function getAllToJson()
    {
        $jsonArray = array();
        foreach (self::getAll() as $item) 
        {
            array_push($jsonArray, json_decode($item->toJson(), true));
        }
        return json_encode($jsonArray);
    }


    public function toJson()
	{
		return json_encode(array(
			'idStation'=>$this->idStation,
			'description'=>$this->description
		));
	}
}
?>