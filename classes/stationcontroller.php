<?php
require_once("models/station.php");

//GET method
if($_SERVER['REQUEST_METHOD']=='GET')
{
   if(isset($_GET['idStation']))
   {
       try{
            $station =  new Station($_GET['idStation']);
            echo json_encode(array(
                'status' => 0,
                'station' => json_decode($station->toJson())
            ));
       }catch(RecordNotFoundException $ex){
            echo json_encode(array(
                'status' => 3,
                'message' => $ex->getMessage()
            ));
       }
   }
   else 
   {
       echo json_encode(array(
            'status' => 0,
            'stations' => json_decode(Station::getAllToJson())
        ));
   }
}

//POST Method
if ($_SERVER['REQUEST_METHOD'] == 'POST')
{        
        if(isset($_POST['idStation']) && isset($_POST['description']))
        {        
            try
            {
                $station = new Station($_POST['idStation'], $_POST['description']);
            }catch(RecordNotFoundException $ex){
                echo json_encode(array(
                   'status' => 3,
                   'message' => $ex->getMessage()
                ));
                die;
            }
            $station = new Station($_POST['idStation'], $_POST['description']); 
            if($station->add()){
                echo json_encode(array(
                    'status' => 0,
                    'message' => 'Station added successfully'
                ));
            } 
            else {
                echo json_encode(array(
                    'status' => 2,
                    'message' => 'Station was not added to the database'
                ));
            }                     
        }             
        else{
            echo json_encode(array(
                'status' => 1,
                'message' => 'missing parameters'
            ));
        }
}
